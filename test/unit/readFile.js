const chai = require('chai');
const expect = chai.expect;
const fs = require('fs');
const readFileUtils = require('../../utils/readFile');

describe('Twitter feed read file utils', function () {

    before("Add test file", function (done) {
        global.filePath = 'test.txt';
        global.fileType = 'users';
        global.fileContents = 'List of users and followers';
        fs.writeFile(global.filePath, global.fileContents, function (err) {
            if (err) throw err;
            done();
        });
    });

    describe('Check if file exists', function () {
        it('should return true for a file that exists', function (done) {
            this.timeout(20000);
            readFileUtils.doesFileExist(global.filePath, global.fileType, function (asyncError, callback) {
                expect(callback).to.equal(global.filePath);
                done();
            });
        });
        it('should throw an error for a file that does not exist', function (done) {
            this.timeout(20000);
            let filePath = "phantom.txt";
            let fileNotFoundMsg = "Error reading text file for " + global.fileType + ". The following file was not found: " + filePath;
            readFileUtils.doesFileExist(filePath, global.fileType, function (asyncError, callback) {
                expect(asyncError).to.equal(fileNotFoundMsg);
                done();
            });
        });
    });

    describe('Read files', function () {
        it('should throw an error for a file that could not be read', function (done) {
            readFileUtils.readFile(global.filePath, function (asyncError, callback) {
                expect(callback.toString()).to.equal(global.fileContents);
                done();
            });
        });
        it('should throw an error when reading a file that does not exist', function (done) {
            this.timeout(20000);
            let filePath = "phantom.txt";
            let readFileErrorMsg = "The following file could not be read: " + filePath + ". ";
            let err = "Error: ENOENT: no such file or directory, open 'phantom.txt'"
            readFileUtils.readFile(filePath, function (asyncError, callback) {
                expect(asyncError).to.equal(readFileErrorMsg + err);
                done();
            });
        });
    });

    describe('Seven bit ASCII check', function (done) {
        it('should return the contents of a seven bit ASCII file', function (done) {
            let testString = "This is a string";
            let testBuffer = Buffer.from(testString);
            this.timeout(20000);
            readFileUtils.isASCII(testBuffer, global.filePath, function (error, callback) {
                expect(error).to.equal(null);
                expect(callback).to.equal(testBuffer);
                done();
            });
        });
        it('should return an error if the file is not seven bit ASCII', function (done) {
            let testBinary = "��˰!1�H��1�1����!H�=u�!�";
            let testBuffer = Buffer.from(testBinary);
            let fileTypeErrorMsg = "The following file is not in the accepted format: " + global.filePath;
            this.timeout(20000);
            readFileUtils.isASCII(testBuffer, global.filePath, function (error, callback) {
                expect(error).to.equal(fileTypeErrorMsg);
                done();
            });
        });
    });

    describe('Convert file contents to array of lines', function () {
        it('should convert the contents of a file to an array', function (done) {
            this.timeout(20000);
            let contents = "Ward follows Alan\nAlan follows Martin\nWard follows Martin, Alan";
            readFileUtils.convertToArray(contents, function (asyncError, contentArray) {
                expect(contentArray).to.be.an('array');
                done();
            })
        });
        it('should throw an error if contents of a file cannot be converted to an array', function (done) {
            this.timeout(20000);
            let contents = null;
            readFileUtils.convertToArray(contents, function (asyncError, contentArray) {
                expect(asyncError).to.not.equal(null);
                done();
            })
        });
    });

    describe('Parse file async waterfall', function () {
        it('should use the async waterfall to convert a text file to an array', function (done) {
            this.timeout(20000);
            readFileUtils.parseFile(global.filePath, global.fileType, function (asyncError, contentArray) {
                expect(contentArray).to.be.an('array');
                done();
            })
        });
        it('should throw an async waterfall error if an error is thrown in waterfall functions', function (done) {
            this.timeout(20000);
            let filePath = "phantom.txt";
            readFileUtils.parseFile(filePath, global.fileType, function (asyncError, contentArray) {
                expect(asyncError).to.not.equal(null);
                done();
            })
        });
    });

    after("Remove test data", function (done) {
        this.timeout(20000);
        console.log("removing test file");
        try {
            fs.unlinkSync(filePath);
            done();
        } catch (err) {
            console.error(err);
        }
    });

});