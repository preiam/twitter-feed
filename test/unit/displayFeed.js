const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const assert = require('assert');
const displayFeed = require('../../utils/displayFeed');

describe('Displaying the twitter feed in the console', function () {
    before('create user collection', function (done) {
        this.timeout(20000);
        global.users = {
            draco: { username: 'draco', follows: new Set(['lucius', 'bellatrix']), myTweets: [] },
            lucius: { username: 'lucius', follows: new Set([]), myTweets: [0] },
            narcissa: { username: 'narcissa', follows: new Set([]), myTweets: [] },
            bellatrix: { username: 'bellatrix', follows: new Set([]), myTweets: [1] },
        };
        global.tweets = {
            '0': {
                username: "lucius",
                tweetString: "Your parents were meddlesome fools too. Mark my words, Potter. One day soon you are going to meet the same sticky end."
            },
            '1': {
                username: "bellatrix",
                tweetString: "Oh, he knows how to play, little bitty baby Potter."
            },

        };
        global.usernamesSet = new Set(['bellatrix', 'draco', 'lucius', 'narcissa']);
        done();
    });

    describe('Creating username set', function () {
        it('should return a new Set of usernames', function (done) {
            this.timeout(20000);
            displayFeed.createUsernameSet(global.users, global.tweets, function (asyncError, users, tweets, usernamesSet) {
                expect(usernamesSet.size).to.equal(4);
                done();
            })
        });
        it('should return an error when no users are passed to createUsernameSet', function (done) {
            this.timeout(20000);
            let users = null;
            displayFeed.createUsernameSet(users, global.tweets, function (asyncError, users, tweets, usernamesSet) {
                expect(asyncError).to.be.an.instanceOf(Error);
                done();
            })

        });
    });

    describe('Finding tweets to display for a user', function () {
        it('should create an ordered array of tweets to display for a user', function (done) {
            this.timeout(20000);
            let username = 'draco';
            displayFeed.findTweetsToDisplay(username, global.users, global.tweets, function (asyncError, username, orderedTweetsToDisplay, tweetCollection) {
                expect(orderedTweetsToDisplay.length).to.equal(2);
                done();
            })
        });
        it('should return an error when no username is passed', function (done) {
            this.timeout(20000);
            let username = null;
            displayFeed.findTweetsToDisplay(username, global.users, global.tweets, function (asyncError, username, orderedTweetsToDisplay, tweetCollection) {
                expect(asyncError).to.be.an.instanceOf(Error);
                done();
            })

        });
    });

    describe('Output to the console', function () {
        it('should output tweets for the user and the users he/she follows', function (done) {
            this.timeout(20000);
            let spy = sinon.spy(console, 'log');
            let username = 'draco';
            let orderedTweetsToDisplay = [0, 1];
            displayFeed.outputToConsole(username, orderedTweetsToDisplay, global.tweets, function (asyncError) {
                assert(spy.calledWith("Draco"));
                assert(spy.calledWith('\t@' + "Lucius: Your parents were meddlesome fools too. Mark my words, Potter. One day soon you are going to meet the same sticky end."));
                assert(spy.calledWith('\t@' + "Bellatrix: Oh, he knows how to play, little bitty baby Potter."));
                console.log.restore();
                done();
            });

        });
    });

    describe('show user tweets waterfall function', function () {
        it('should return with no error for a successful run', function (done) {
            this.timeout(20000);
            displayFeed.showUserTweets(global.users, global.tweets, global.usernamesSet, function (asyncError) {
                expect(asyncError).to.equal(null);
                done();

            });
        });
    });
});