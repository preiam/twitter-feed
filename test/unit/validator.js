const chai = require('chai');
const expect = chai.expect;
const validator = require('../../utils/validator');

describe('Twitter feed validation utils', function () {
    describe('Validating a line in the user text file', function () {
        it('should return an object with the username and an array of followers if valid', function (done) {
            this.timeout(20000);
            let string1 = 'Harry follows Sirius';
            let string2 = 'Minerva follows Mad-Eye, Albus';
            let string3 = 'Draco follows Crabbe and Goyle, Viktor, Myrtle';
            validator.validateUserTextString(string1, function (asyncError, result1) {
                expect(result1.isValid).to.be.true;
                expect(result1.username).to.equal('Harry');
                expect(result1.follows).to.equal('Sirius');
            });            
            validator.validateUserTextString(string2, function (asyncError, result2) {
                expect(result2.isValid).to.be.true;
                expect(result2.username).to.equal('Minerva');
                expect(result2.follows).to.equal('Mad-Eye, Albus');
            });            
            validator.validateUserTextString(string3, function (asyncError, result3) {
                expect(result3.isValid).to.be.true;
                expect(result3.username).to.equal('Draco');
                expect(result3.follows).to.equal('Crabbe and Goyle, Viktor, Myrtle');
            });
            done();
        });
        it('should return an object with an error message if invalid', function (done) {
            this.timeout(20000);
            let string1 = 'Harry folows Sirius';
            let string2 = 'Harryfollows Sirius';
            let string3 = 'Harry followsSirius';
            validator.validateUserTextString(string1, function (asyncError1, result) {
                expect(asyncError1.isValid).to.be.false;
            });            
            validator.validateUserTextString(string2, function (asyncError2, result) {
                expect(asyncError2.isValid).to.be.false;
            });            
            validator.validateUserTextString(string3, function (asyncError3, result) {
                expect(asyncError3.isValid).to.be.false;
            });
            done();
        });
    });

    describe('Validating a username', function () {
        /*  
            A username will throw a validation error and be discarded if it:
            1. is longer than 15 characters
            2. contains any whitespace or non-alphanumeric characters with the exception of underscores
            NOTE: a username is not case-sensitive and will always be displayed in title case  
        */
        it('should return true for a valid username', function (done) {
            this.timeout(20000);
            let string = 'Harry_Potter';
            validator.validateUsername(string, function (asyncError, result) {
                expect(result).to.equal(true);
                done();
            });            
        });
        it('should return false for an username with special characters', function (done) {
            let string = 'Prof-Severus-Snape';
            validator.validateUsername(string, function (asyncError, result) {
                expect(asyncError).to.equal(false);
                done();
            });            
        });
        it('should return false for an username longer than 15 character', function (done) {
            let string = 'XenophiliusLovegood';
            validator.validateUsername(string, function (asyncError, result) {
                expect(asyncError).to.equal(false);
                done();
            });            
        });
        it('should return false for an username with whitespace', function (done) {
            let string = 'Molly Weasley';
            validator.validateUsername(string, function (asyncError, result) {
                expect(asyncError).to.equal(false);
                done();
            });            
        });
    });

    /* 
        Only tweets added to the tweet text file in the following format will be parsed:
        1.  <user>><space><tweet>
        2. a new tweet entry must start on a new line
    */
    describe('Validating a line in the tweet text file', function () {
        it('should return an object with the username and tweet if valid', function (done) {
            this.timeout(20000);
            let username1 = "HarryPotter";
            let tweet1 = "Scetum sempra!!!!"
            let tweetText1 = username1 + '> ' + tweet1;
            let users = [];
            validator.validateTweetString(tweetText1, users, function (asyncError, result1) {
                expect(result1.isValid).to.be.true;
                expect(result1.username).to.equal(username1);
                expect(result1.tweetString).to.equal(tweet1);
                done();
            });
        });
        it('should return an object with an error message if tweet string is longer than 140 characters', function (done) {
            this.timeout(20000);
            let username2 = "Severus";
            let tweet2 = "There will be no foolish wand-waving or silly incantations in this class. As such, I don't expect many of you to appreciate the subtle science and exact art that is potion-making. However, for those select few who possess the predisposition, I can teach you how to bewitch the mind and ensnare the senses. I can tell you how to bottle fame, brew glory, and even put a stopper in death."
            let tweetText2 = username2 + '>' + tweet2;
            let users = [];
            validator.validateTweetString(tweetText2, users, function (asyncError, result2) {
                expect(asyncError.isValid).to.be.false;
                done();
            });
        });
        it('should return an object with an error message if entire tweet text format invalid', function (done) {
            this.timeout(20000);
            let username2 = "HarryPotter";
            let tweet2 = "Scetum sempra!!!!"
            let tweetText2 = username2 + '>' + tweet2;
            let users = [];
            validator.validateTweetString(tweetText2, users, function (asyncError, result2) {
                expect(asyncError.isValid).to.be.false;
                done();
            });
        });
    });

    describe('Validating all usernames in the user text object', function () {
        it('should return a validated user text object if the username is valid', function (done) {
            this.timeout(20000);
            let username = "Hermione"
            let follows = "Ron, Harry, Luna"
            let userTextObject = {
                username: username,
                follows: follows
            };
            validator.validateAllUsernames(userTextObject, function (asyncError, result) {
                expect(result.username).to.equal(username);
                expect(result.follows).to.be.an('array');
                done();
            });
        });
        it('should return a null if the username is invalid', function (done) {
            this.timeout(20000);
            let username = "HermiOn$"
            let follows = "R*n, H@rry, L^na"
            let userTextObject = {
                username: username,
                follows: follows
            };
            validator.validateAllUsernames(userTextObject, function (asyncError, result) {
                expect(result.username).to.equal(null);
                expect(result.follows).to.be.empty;
                done();
            });
        });
        it('should ignore follows user validation if follows is empty', function (done) {
            this.timeout(20000);
            let username = "Hermione"
            let follows = "";
            let userTextObject = {
                username: username,
                follows: follows
            };
            validator.validateAllUsernames(userTextObject, function (asyncError, result) {
                expect(asyncError).to.be.equal(null);
                expect(result.username).to.equal(username);
                expect(result.follows).to.be.empty;
                done();
            });
        });
        it('should throw an async parallel error if userTextObject is null', function (done) {
            this.timeout(20000);
            let userTextObject = {};
            validator.validateAllUsernames(userTextObject, function (asyncError, result) {
                expect(asyncError).to.equal(null);
                expect(result.username).to.equal(undefined);
                expect(result.follows).to.be.empty;
                done();
            });
        });
    });
});