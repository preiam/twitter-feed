const chai = require('chai');
const expect = chai.expect;
const tweet = require('../../factories/tweet');

describe('Tweet Factory and Functions', function () {
    beforeEach("Add test file", function (done) {
        this.timeout(20000);
        global.tweetTextObject = {
            username: 'dumbledore',
            tweetString: 'It is our choices, Harry, that show what we truly are, far more than our abilities.'
        };
        global.users = {
            dumbledore: { username: 'dumbledore', follows: new Set (['hagrid', 'mcgonagall']), myTweets: [] },
        };
        global.username = 'dumbledore';
        done();
    });
    it('should create a tweet collection abd update a user\'s myTweets given a tweet text object and user collection', function(done) {
        this.timeout(20000);
        
        tweet.createTweetObject(global.tweetTextObject, global.users, function(error, tweetCollection, usersWithTweets) {
            expect(Object.keys(tweetCollection).length).to.equal(1);
            expect(usersWithTweets[global.username]["myTweets"].length).to.equal(1);
        });
        done();
    });
    it('should throw an error when the username in a tweet text object does not exist in the tweet collection', function(done) {
        this.timeout(20000);
        let tweetTextObject = {
            username: 'dobby',
            tweetString: 'Bad dobby!!'
        };
        tweet.createTweetObject(tweetTextObject, global.users, function(error, tweetCollection, usersWithTweets) {
            let errorMessage = 'The following user does not exist: ' + tweetTextObject.username + '. Tweet will be discarded.';
            expect(error.errorMessage).to.equal(errorMessage);
            done();
        });        
    })
})