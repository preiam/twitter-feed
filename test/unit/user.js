const chai = require('chai');
const expect = chai.expect;
const user = require('../../factories/user');

describe('User Factory and Functions', function () {
    beforeEach("Add test file", function (done) {
        this.timeout(20000);
        global.userTextObject = {
            username: '',
            follows: []
        };
        global.username = "Ginny";
        global.follows1 = "Harry";
        global.follows2 = "Ron";
        done();
    });

    describe('Adding new user follows', function () {
        it('should not add user to collection if null', function (done) {
            this.timeout(20000);
            global.userTextObject['username'] = null;
            user.addFollows(global.userTextObject, function (error, result) {
                expect(Object.keys(result).length).to.equal(0);
            })
            done();
        });
        it('should not add user to follows if follows user = user', function (done) {
            this.timeout(20000);
            global.userTextObject['username'] = global.username;
            global.userTextObject['follows'] = [global.follows1, global.follows2, global.username];
            user.addNewUsers(global.userTextObject, function (error, result) {
                user.addFollows(global.userTextObject, function (error, result) {
                    expect(result[global.username.toLowerCase()]['follows'].has(global.username)).to.equal(false);
                    done();
                })                
            });            
        });
    });
    
    describe('Adding new users', function () {
        it('should not add user if username is null', function (done) {
            this.timeout(20000);
            global.userTextObject['username'] = null;
            global.userTextObject['follows'] = [global.follows1, global.follows2];
            user.addNewUsers(global.userTextObject, function (error, result) {
                expect(error).to.equal(null);
                expect(result.username).to.equal(null);
                expect((result.follows).length).to.equal(2);
            });
            done();
        });
    });

    describe('Create a new users waterfall', function () {
        it('should create a collection of new users with users he/she follows', function (done) {
            this.timeout(20000);
            global.userTextObject['username'] = global.username;
            global.userTextObject['follows'] = [global.follows1, global.follows2];
            user.createUser(global.userTextObject, function (err, result) {
                expect(err).to.equal(null);
                expect(Object.keys(result).length).to.equal(3);
                expect(Object.keys(result)[0]).to.equal(global.follows1.toLowerCase());
                expect(result[username.toLowerCase()]['follows'].size).to.equal(2);
                done();
            })

        });
    });
});