const async = require('async');
const LoggerService = require('./config/logger');
const logger = new LoggerService('twitterFeed');
const readFileUtils = require('./utils/readFile');
const validator = require('./utils/validator');
const user = require('./factories/user');
const tweet = require('./factories/tweet');
const displayFeed = require('./utils/displayFeed');

const userFile = process.argv[2];
const tweetFile = process.argv[3];

/*  
    read the files provided as arguments and save each line as an item in an array  
*/
function saveFilesToArrays(userFile, tweetFile, callback) {
    async.parallel([
        // parse user text file
        function (done) {
            readFileUtils.parseFile(userFile, "users", done);
        },
        // parse tweet text file
        function (done) {
            readFileUtils.parseFile(tweetFile, "tweets", done);
        }
    ], function (error, results) {
        if (error) {
            logger.debug("Error reading files", error);
            let errorMessage = "Error reading files. " + error + ". Please confirm that the files being passed are in the accepted format.";
            return callback(errorMessage);
        }
        logger.info("Successfully read and saved text files to arrays.");
        let userTextArray = results[0];
        let tweetTxtArray = results[1];
        return callback(null, userTextArray, tweetTxtArray);
    });
}

/*  
    parse each item in the userTextArray, validate and then 
    create a collection of users and the users he/she follows  
*/
function createUserCollection(userTextArray, tweetTxtArray, callback) {
    let userCollection = {};
    logger.info("Parsing users in user text array...");
    async.mapSeries(userTextArray, parseUserText, function (err, result) {
        if (!err) {
            logger.info("Successfully parsed user text array. Validation completed and user collection created.");
            return callback(null, tweetTxtArray, userCollection);
        }
    });

    function parseUserText(string, done) {
        async.waterfall([
            async.constant(string),
            validator.validateUserTextString,
            validator.validateAllUsernames,
            user.createUser
        ], function (err, result) {
            if (err) {
                logger.debug("Error validating text in user file", err.errorMessage);
                return done(null, err.errorMessage);
            }
            userCollection = result;
            return done(null, result);
        })
    }
}


/*  
    parse each item in the tweetTextArray, validate and then 
    create a collection of tweets  
*/

function createTweetCollection(tweetTextArray, userCollection, callback) {
    let tweets = [];
    let usersAndTweets = [];
    logger.info("Parsing tweets in tweet text array...");
    async.eachSeries(tweetTextArray, function (tweetText, callbackEach) {
        async.waterfall([
            async.constant(tweetText, userCollection),
            validator.validateTweetString,
            tweet.createTweetObject
        ], function (err, tweetCollection, usersWithTweets) {
            if (err) {
                logger.debug("Error validating text in tweet file", err.errorMessage);       
                return callbackEach();
            }
            tweets = tweetCollection;
            usersAndTweets = usersWithTweets;
            return callbackEach();
        })
    }, function (err) {
        if (err) {
            logger.debug("Error creating tweet collection", err);
            return callback(null, usersAndTweets, tweets);
        }
    });
    logger.info("Successfully parsed tweet text array. Validation complete and twitter collection created.");
    return callback(null, usersAndTweets, tweets);
}

/*  
    for each user in the user file passed as an argument, 
    display the users tweets and the tweets of the users he/she follows 
    in the order the tweets appear in the tweet file  
*/
function displayTwitterFeed(users, tweets) {
    async.waterfall([
        async.constant(users, tweets),
        displayFeed.createUsernameSet,
        displayFeed.showUserTweets
    ], function (err) {
        if (err) {
            logger.error("Error displaying twitter feed ", err);
        }
        logger.info("Display twitter feed complete");
        return;
    });
}

async.waterfall([
    async.constant(userFile, tweetFile),
    saveFilesToArrays,
    createUserCollection,
    createTweetCollection,
    displayTwitterFeed
], function (err) {
    if (err) {
        logger.error("Error displaying twitter feed ", err);
        return;
    }
});

exports.saveFilesToArrays = saveFilesToArrays;
exports.displayTwitterFeed = displayTwitterFeed;
