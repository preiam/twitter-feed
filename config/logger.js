const winston = require('winston');
const config = require('./config');
const moment = require('moment');
const log_level = config.get('LOG_LEVEL');

const dateFormat = function () {
    const formatter= 'DD-MM-YYYY HH:mm:ss';
    const date = new Date(Date.now());
    const time = moment(date).format(formatter);
    return time;
}

class LoggerService {
    constructor(route) {
        this.log_data = null;
        const logger = winston.createLogger({
            name: 'console.info',
            level: log_level,
            transports: [
                new winston.transports.Console(),
                new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
                new winston.transports.File({ filename: './logs/complete.log' })
            ],
            format: winston.format.printf(function(info){
                let message = `${dateFormat()} | ${info.level.toUpperCase()} | ${route} | ${info.message} | `
                message = info.obj ? message + `${JSON.stringify(info.obj)} | ` : message
                message = this.log_data ? message + `log_data:${JSON.stringify(this.log_data)} | ` : message
                return message
            })
        });
        this.logger = logger
    }
    setLogData(log_data) {
        this.log_data = log_data
    }
    async info(message) {
        this.logger.log('info', message);
    }
    async info(message, obj) {
        this.logger.log('info', message, {
            obj
        })
    }
    async debug(message) {
        this.logger.log('debug', message);
    }
    async debug(message, obj) {
        this.logger.log('debug', message, {
            obj
        })
    }
    async error(message) {
        this.logger.log('error', message);
    }
    async error(message, obj) {
        this.logger.log('error', message, {
            obj
        })
    }
}
module.exports = LoggerService

