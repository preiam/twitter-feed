const dotenv = require('dotenv');
dotenv.config();

function get(setting) {
  const config = process.env[setting] || null;
  if (!config) {
    throw new Error("Config value '" + setting + "' not set.");
  }
  return config;
}

module.exports.get = get
