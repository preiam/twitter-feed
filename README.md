# twitter-feed
A javascript program that simulates a twitter-like feed with NodeJS

## Instructions to run

1. Clone/Download twitter-feed
2. Navigate to the twitter-feed root folder
3. Install the necessary node modules, run "npm install"
4. To run the application, run "npm start <file1> <file2>", where file1 is the user text file and file2 is the tweet file
5. To run the unit tests, run "npm test"

Log files are written to "twitter-feed/logs/".
To change the log level, edit the "LOG_LEVEL" variable in "twitter-feed/.env".

## Assumptions	

1. A user can follow others users and/or be followed by other users.
	
2. A user's username cannot be more than 15 characters.
	
3. A user's username must be unique and can only contain alphanumeric characters (letters A-Z, numbers 0-9) with the exception of underscores.
	
4. A comma and space is used to separate all usernames.
	
5. A username is not case-sensitive.
	
6. If a username does not conform to the accepted format it will not be included and the user will not exist.

7. Only users/followers added to the user text file in the following format will be parsed:
	- user<space>follows<space>comma separated list of users they follow...
	- a new user entry , with the users he/she follows, must start on a new line  
		
8. A user cannot follow him/herself.
	
9. Only tweets added in the following format will be included:
	- <user>><space><tweet>    
	- new tweet entries must start on a new line
	- the length of a tweet cannot exceed 140 characters
	- a tweet can contain special characters
		
10. Each user sees his/her own tweets and the tweets of the users he/she follows.

11. A tweet by a user that does not exist in the users file will not be displayed.

12. The user and tweet text files must be seven-bit encoded ASCII files