const _ = require('lodash');
const async = require('async');
const userCollection = {};
const usernameCollection = new Set();
const LoggerService = require('../config/logger');
const logger = new LoggerService('user');

function userFactory(username) {
    const user = {};
    user.username = username;
    user.follows = new Set();
    user.myTweets = [];

    return {
        username: user.username,
        follows: user.follows,
        myTweets: user.myTweets,
    }
}

/*
    If user does not already exist, create a user object with keys "username", "follows" and "myTweets"
    Add user object to a collection of users
    The users before and after the word "follows" in the user text file line will be created, but not duplicated
*/

function addNewUsers(userTextObject, callback) {
    const username = userTextObject.username;
    const followsArray = userTextObject.follows;
    let allUsersArray = [];
    if (username == null) {
        logger.debug("Null username detected from malformed text.");
        allUsersArray = followsArray;
    } else {
        allUsersArray = followsArray.concat([username]);
    }
    logger.debug("Users to create if doesn't exist", allUsersArray);
    async.eachSeries(allUsersArray, function (value, callbackEach) {
        let username = value;
        let doesUserExist = usernameCollection.has(username.toLowerCase());
        if (!doesUserExist) {
            let user = userFactory(username);
            let usernameLowerCase = username.toLowerCase();
            userCollection[usernameLowerCase] = user;
            usernameCollection.add(usernameLowerCase);
            return callbackEach();
        }
        return callbackEach();
    }, function (err) {
        if (err) {
            logger.error(err);
        }
        return callback(null, userTextObject);
    });
};

/*
    If username is valid in line of user text file, add follows users to "follows" key in user object
    NOTE: A user cannot follow himself/herself
*/

function addFollows(userTextObject, callback) {
    let username = userTextObject.username;
    // if username is undefined/null, there are no users to add to follows, exit
    if (username == null) {
        logger.debug("Null username. No user to add follows.");
        callback(null, userCollection);
        return;
    }
    let followsArray = userTextObject.follows;
    let usernameLowerCase = username.toLowerCase();
    async.eachSeries(followsArray, function (value, callbackEach) {
        let followsUser = value.toLowerCase();
        // a user cannot follow himself/herself
        if (followsUser === usernameLowerCase) {
            logger.debug("Follows user matches username. Cannot add to follows.", followsUser);
            return callbackEach(null);
        }
        (userCollection[usernameLowerCase]["follows"]).add(value.toLowerCase());
        return callbackEach(null);
        
        return;
    }, function (err) {
        if (err) {
            logger.error(err);
        }
        return callback(null, userCollection);
    });
}

function createUser(userTextObject, callback) {
    async.waterfall([
        async.constant(userTextObject),
        addNewUsers,
        addFollows
    ], function (err, result) {
        if (err) {
            logger.log(err);
            return calllback(err);
        }
        return callback(null, result);
    });
}

exports.addNewUsers = addNewUsers;
exports.addFollows = addFollows;
exports.createUser = createUser;