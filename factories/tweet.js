const LoggerService = require('../config/logger');
const logger = new LoggerService('tweet');

const tweetCollection = {};

function tweetFactory(username, tweetString) {
    const tweet = {};
    tweet.username = username;
    tweet.tweetString = tweetString;

    return {
        username: tweet.username,
        tweetString: tweet.tweetString
    }
}

/*
    Create a tweet object with keys "username" and "tweetString"
    Add tweet object to a collection of tweets 
*/

function createTweetObject(tweetTextObject, users, callback) {   
    let userCollection = users;
    let usernameCollection = new Set(Object.keys(userCollection));
    const username = tweetTextObject.username;
    
    let usernameLowerCase = username.toLowerCase();
    let doesUserExist = usernameCollection.has(usernameLowerCase);
    logger.debug("Username in tweet",usernameLowerCase);
    // only add tweets to tweetCollection for users that exist
    if (doesUserExist) {
        const tweetString = tweetTextObject.tweetString;
        let tweetCollectionLength = Object.keys(tweetCollection).length;
        let tweetIndex = tweetCollectionLength;

        let tweet = tweetFactory(usernameLowerCase, tweetString);
        tweetCollection[tweetIndex] = tweet;
        let usersWithTweets = users;
        usersWithTweets[usernameLowerCase]["myTweets"].push(tweetIndex);
        return callback(null, tweetCollection, usersWithTweets);
    }

    let errorMsg = "The following user does not exist: " + username + ". Tweet will be discarded." ;
    let invalidUser = {
        errorMessage: errorMsg
    };    
    return callback(invalidUser);
}

exports.createTweetObject = createTweetObject;