const fs = require('fs');
const async = require('async');
const LoggerService = require('../config/logger');
const logger = new LoggerService('readFile');

function doesFileExist(filePath, fileType, callback) {
    if (fs.existsSync(filePath)) {
        callback(null, filePath);
        return;
    }
    let fileNotFoundMsg = "Error reading text file for " + fileType + ". The following file was not found: " + filePath;
    callback(fileNotFoundMsg);
    return;
}

function readFile(filePath, callback) {
    fs.readFile(filePath, function (error, buffer) {
        if (error) {
            let readFileErrorMsg = "The following file could not be read: " + filePath + ". " + error;
            return callback(readFileErrorMsg);
        }
        return callback(null, buffer, filePath);
    });
}

function isASCII(buffer, filePath, callback) {
    for (var i = 0, len = buffer.length; i < len; i++) {
        if (buffer[i] > 127) {
            let fileTypeErrorMsg = "The following file is not in the accepted format: " + filePath;
            logger.debug("Incorrect file format detected.");
            return callback(fileTypeErrorMsg);
        }
    }    
    return callback(null, buffer);
}

function convertToArray(data, callback) {
    let contentArray;
    try {
        contentArray = data.toString().split("\n");
        return callback(null, contentArray);
    } catch (err) {
        let errorMsg = "Error converting to array: " + err;
        return callback(errorMsg);
    }
}

function parseFile(filePath, fileType, callback) {
    async.waterfall([
        async.constant(filePath, fileType),
        doesFileExist,
        readFile,
        isASCII,
        convertToArray
    ], function (err, result) {
        if (err) {
            return callback(err);
        }
        return callback(null, result);
    });
}

exports.readFile = readFile;
exports.doesFileExist = doesFileExist;
exports.isASCII = isASCII;
exports.convertToArray = convertToArray;
exports.parseFile = parseFile;