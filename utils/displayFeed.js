const async = require('async');
const LoggerService = require('../config/logger');
const logger = new LoggerService('displayFeed');

/*
    Find all users in user collection and create a username set
*/

function createUsernameSet(users, tweets, callback) {
    try {
        const userCollection = users;
        const userCollectionKeys = Object.keys(userCollection);
        const sortedUsernameArray = userCollectionKeys.sort();
        logger.debug("Display tweets for the following users and users he/she follows", sortedUsernameArray);
        const usernamesSet = new Set(sortedUsernameArray);
        return callback(null, users, tweets, usernamesSet);
    } catch (error) {
        return callback(error);
    }
}

/*
    For each user, create an ordered array of his/her tweets and the tweets of the users he/she follows
*/
function findTweetsToDisplay(username, userCollection, tweetCollection, callback) {
    try {
        let usernameToDisplay = userCollection[username]['username'];
        let userTweets = userCollection[username]['myTweets'];
        let userFollows = userCollection[username]['follows'];

        let tweetsToDisplay = userTweets;
        async.eachSeries(userFollows, function (value, callbackEach) {
            let userFollows = value;
            let followsTweets = userCollection[userFollows]['myTweets'];
            tweetsToDisplay = tweetsToDisplay.concat(followsTweets);
            return callbackEach();
        });
        let tweetsTypedArray = new Uint32Array(tweetsToDisplay);
        let orderedTweetsToDisplay = tweetsTypedArray.sort();
        return callback(null, username, orderedTweetsToDisplay, tweetCollection);
    }
    catch (error) {
        return callback(error);
    }
}

/*
    Display console output of each user and the tweets he/she will see in a twitter feed
*/

function outputToConsole(username, orderedTweetsToDisplay, tweets, callback) {
    let usernameToDisplay = username;
    let titleCaseUsername = usernameToDisplay.charAt(0).toUpperCase() + usernameToDisplay.slice(1);
    let tweetsToDisplay = orderedTweetsToDisplay;
    let tweetCollection = tweets;

    console.log(titleCaseUsername); // DO NOT REMOVE
    async.eachSeries(tweetsToDisplay, function (value, callbackEach) {
        let tweetindex = value;
        let tweet = tweetCollection[tweetindex]['tweetString'];
        let tweetUser = tweetCollection[tweetindex]['username'];
        let titleCaseTweetUser = tweetUser.charAt(0).toUpperCase() + tweetUser.slice(1);
        let displayString = '\t@' + titleCaseTweetUser + ': ' + tweet;
        console.log(displayString); //DO NOT REMOVE
        return callbackEach();
    }, function (err) {
        if (err) {
            logger.debug("Error sending ouput to console", err);
            return callback(err);
        }
        return callback();
    });
}

function showUserTweets(users, tweets, usernamesSet, callback) {
    const userCollection = users;
    const tweetCollection = tweets;
    const usernamesArray = Array.from(usernamesSet);
    async.forEachOf(usernamesArray, function (value, key, callbackForEach) {
        let username = value;
        async.waterfall([
            async.constant(username, userCollection, tweetCollection),
            findTweetsToDisplay,
            outputToConsole
        ], function (err, result) {
            if (err) {
                return callbackForEach(err);
            }
            return callbackForEach(null, result);
        });
    });
    return callback(null);
}

exports.createUsernameSet = createUsernameSet;
exports.showUserTweets = showUserTweets;
exports.findTweetsToDisplay = findTweetsToDisplay;
exports.outputToConsole = outputToConsole;