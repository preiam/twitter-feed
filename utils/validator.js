const async = require('async');
const LoggerService = require('../config/logger');
const logger = new LoggerService('validator');

/*  
    Only users/followers added to the user text file in the following format will be parsed:
    1. user<space>follows<space>comma separated list of users they follow...
    2. a new user entry , with the users he/she follows, must start on a new line  
*/

function validateUserTextString(string, callback) {
    const userRegEx = /^(.*)(\sfollows\s)(.*)/;
    let patternMatch = string.match(userRegEx);
    if (patternMatch) {
        let username = patternMatch[1];
        let text = patternMatch[2];
        let follows = patternMatch[3];
        let validResult = {
            isValid: true,
            username: username,
            follows: follows
        };
        return callback(null, validResult);
    }
    let invalidResult = {
        isValid: false,
        errorMessage: 'The following line in the users text file is malformed and will be discarded: ' + string
    }
    return callback(invalidResult)
}

/*  
    A username will throw a validation error and be discarded if it:
    1. is longer than 15 characters
    2. contains any whitespace or non-alphanumeric characters with the exception of underscores
    NOTE: a username is not case-sensitive and will always be displayed in title case  
*/

function validateUsername(username, callback) {
    if (/^\w{1,15}$/.test(username)) {
        return callback(null, true);
    }
    logger.debug("Invalid username will not be added: ", username);
    return callback(false);
}

/*
    Validate all usernames in the user text object created from a validate line in the user text file
*/

function validateAllUsernames(userTxtObject, callback) {
    let validatedUserTextObject = {};
    validatedUserTextObject['follows'] = [];
    let userToValidate = userTxtObject.username;
    async.parallel([
        // Parallel function 1 - validate username
        function (callbackFunc1) {
            validateUsername(userToValidate, function (err, isValid) {
                validatedUserTextObject['username'] = isValid ? userTxtObject.username : null;
                return callbackFunc1(null, validatedUserTextObject);
            });
        },

        // Parallel function 2 - validate follows usernames
        function (callbackFunc2) {
            if (userTxtObject.follows == null) {   
                logger.debug("No follows in user text object to validate");
                return callbackFunc2(null);
            }

            let hasFollows = (userTxtObject.follows).length;
            if (hasFollows == 0) {
                return callbackFunc2(null);
            }

            let followsArray = (userTxtObject.follows).split(", ");
            async.eachSeries(followsArray, function (key, callbackEach) {
                let followsUser = key;
                validateUsername(followsUser, function (err, isValid) {
                    let isFollowsValid = isValid;
                    if (isFollowsValid) {
                        (validatedUserTextObject['follows']).push(followsUser);
                        return callbackEach();
                    }
                    return callbackEach();
                });
            }, function (err) {
                if (err) {
                    logger.debug("Error validating follows array", err);
                }
                logger.debug("Validated user object from text", validatedUserTextObject);
                return callbackFunc2(null, validatedUserTextObject)
            });
        }
    ], function (err, results) {
        if (err) {
            logger.debug("Error in async parallel", err);
            return callback(err);
        }
        return callback(null, results[0]);
    });
}

/* 
    Only tweets added to the tweet text file in the following format will be parsed:
    1.  <user>><space><tweet>
    2. a new tweet entry must start on a new line
    3. the length of a tweet cannot exceed 140 characters
    4. a tweet can contain special characters
*/

function validateTweetString(string, users, callback) {
    logger.debug("Validating tweet text", string);
    let tweetRegEx = /^([^\s]+)(>\s)(.{1,140}$)/;
    let patternMatch = string.match(tweetRegEx);
    if (patternMatch) {
        let username = patternMatch[1];
        let tweet = patternMatch[3];
        let validResult = {
            isValid: true,
            username: username,
            tweetString: tweet
        }
        return callback(null, validResult, users);
    }
    let invalidResult = {
        isValid: false,
        errorMessage: 'The following line in the tweet text file is malformed and will be discarded: ' + string
    }
    logger.debug(invalidResult.errorMessage);
    return callback(invalidResult);
}

exports.validateUsername = validateUsername;
exports.validateUserTextString = validateUserTextString;
exports.validateTweetString = validateTweetString;
exports.validateAllUsernames = validateAllUsernames;

